#ifndef GET_VESSEL_INFO_H
#define GET_VESSEL_INFO_H

#include <string>

int get_vessel_info(const std::string &input_file,
                    const std::string &output_file,
                    bool reverse,
                    const std::string &debug_img_file,
                    const std::string &debug_overlay_img_file,
                    const int path_start_pos[2],
                    const int path_end_pos[2]);

#endif // GET_VESSEL_INFO_H

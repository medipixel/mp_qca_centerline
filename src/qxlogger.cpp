#include "qxlogger.h"

#include <QMutexLocker>
#include <QDateTime>
#include <QFile>
#include <QDir>
#include <QStandardPaths>
#include <QThread>
#include <QCoreApplication>

namespace QxLogger
{
    const QString s_typeUnknownStr("[UNKNOWN]");
    const QString s_typeDebugStr("[DEBUG]");
    const QString s_typeInfoStr("[INFO]");
    const QString s_typeWarningStr("[WARNING]");
    const QString s_typeCriticalStr("[CRITICAL]");
    const QString s_typeFatalStr("[FATAL]");
    const QIODevice::OpenMode s_modeLogFile = (QIODevice::Text | QIODevice::Append | QIODevice::WriteOnly);

    static size_t m_max_bytes;
    static size_t m_max_backup_count;
    static QString m_pathLog;

    void rotateLogfile(const QString& logfile, size_t max_backup_count)
    {
        QFileInfo fiLog(logfile);

        QString surfix = fiLog.suffix();
        QString filename = fiLog.completeBaseName();

        // backup old files
        for(size_t i=max_backup_count; i > 0; i--)
        {
            QString curfilepath = fiLog.absolutePath() + QDir::separator() + QString("%1.%2.%3").arg(filename).arg(i).arg(surfix);
            QString prevfilepath;

            if (i==1)
            {
                prevfilepath = fiLog.absoluteFilePath();
            }
            else
            {
                prevfilepath = fiLog.absolutePath() + QDir::separator() + QString("%1.%2.%3").arg(filename).arg(i-1).arg(surfix);
            }

            if (i==max_backup_count)
            {
                QFile::remove(curfilepath);
            }

            QFile::rename(prevfilepath, curfilepath);
        }
    }

    void messagehandlerLogFile(QtMsgType type, const QMessageLogContext &context, const QString &msg)
    {
        static QMutex s_mutex_filelog;

        QMutexLocker locker(&s_mutex_filelog);

        //check file size and if needed create new log!
        {
            QFile outFileCheck(m_pathLog);
            size_t size = size_t(outFileCheck.size());
            if (size > m_max_bytes) //check current log size
            {
                rotateLogfile(m_pathLog, m_max_backup_count);
            }
        }

        const QString *pTypeStr = &s_typeUnknownStr;
        switch (type)
        {
        case QtFatalMsg:
            pTypeStr = &s_typeFatalStr;
            break;
        case QtCriticalMsg:
            pTypeStr = &s_typeCriticalStr;
            break;
        case QtWarningMsg:
            pTypeStr = &s_typeWarningStr;
            break;
        case QtInfoMsg:
            pTypeStr = &s_typeInfoStr;
            break;
        case QtDebugMsg:
            pTypeStr = &s_typeDebugStr;
            break;
        }

        ulong pid = ulong(QCoreApplication::applicationPid());
        ulong tid = ulong(QThread::currentThreadId());

        QString timeStr(QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss:zzz"));
        QString processStr(QString("[%1, %2]").arg(pid, 1, 16, QChar('0')).arg(tid, 1, 16, QChar('0')));
        QString contextStr(QString("(%1, %2)").arg(QFileInfo(context.file).fileName()).arg(context.line));

        QFile outFile(m_pathLog);
        outFile.open(s_modeLogFile);

        QTextStream stream(&outFile);
        stream << timeStr << " " << (*pTypeStr) << " " << processStr << " " << contextStr << ": " << msg << endl;
    }

    bool initLogFile(const QString &filename, size_t max_bytes, size_t max_backup_count)
    {
        m_max_bytes = max_bytes;
        m_max_backup_count = max_backup_count;

        QFileInfo fiLog = QFileInfo(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation).append(QDir::separator()).append(filename));

        // Create folder for logfiles if not exists
        QDir dirLog = fiLog.absoluteDir();
        dirLog.mkpath(".");

        m_pathLog = fiLog.absoluteFilePath();

        QFile outFile(m_pathLog);
        if(outFile.open(s_modeLogFile))
        {
            qInstallMessageHandler(messagehandlerLogFile);
            return true;
        }

        return false;
    }
}

bool initQxLogger(const QString &filename, size_t max_bytes, size_t max_backup_count)
{
    return QxLogger::initLogFile(filename, max_bytes, max_backup_count);
}

const QString& getQxLoggerPath()
{
    return QxLogger::m_pathLog;
}



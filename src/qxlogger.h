#ifndef QXLOGGER_H
#define QXLOGGER_H

#include <QDebug>

bool initQxLogger(const QString &filename, size_t max_bytes=(1 * 1024 * 1024), size_t max_backup_count=5);
const QString& getQxLoggerPath();

/**
 * How to use log
 *
 * Step 1, initQxLogger
 *
 * #include "qxlogger.h"
 *
 * int main(int argc, char *argv[])
 * {
 *    QApplication app(argc, argv);
 *    QCoreApplication::setApplicationName(NAME_APPLICATION_STR);
 *
 *    // application name을 이용하여서 log path을 생성, setApplicationName()이후에 호출
 *    initQxLogger("dcm_upload_tray.log", (1* 1024 * 1024), 5);
 *
 * Step 2, use log
 *
 * #include <QDebug>
 *
 * qDebug() << "this is debug log";
 * qInfo() << "this is info log";
 */

#endif // QXLOGGER_H

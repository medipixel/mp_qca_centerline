//#include <QtCore>
#include <memory.h>

#include "vessel_info.h"

//#include "qxlogger.h"

// includes from ITK
#include <itkTimeProbe.h>
#include <itkPNGImageIOFactory.h>
#include <itkBMPImageIOFactory.h>
#include <itkTIFFImageIOFactory.h>
#include <itkJPEGImageIOFactory.h>
#include <itkImageFileReader.h>
#include <itkImageFileWriter.h>
#include <itkRescaleIntensityImageFilter.h>
#include <itkCastImageFilter.h>
#include <itkDanielssonDistanceMapImageFilter.h>
#include <itkMaskImageFilter.h>
#include <itkLabelContourImageFilter.h>
#include <itkBinaryContourImageFilter.h>
#include <itkInvertIntensityImageFilter.h>

#include <itkPolyLineParametricPath.h>
//#include <itkSpeedFunctionToPathFilter.h>
#include <itkPathIterator.h>
#include <itkLinearInterpolateImageFunction.h>
#include <itkGradientDescentOptimizer.h>

#include <itkLabelObject.h>
#include <itkLabelMap.h>
#include <itkLabelImageToLabelMapFilter.h>
#include <itkLabelMapOverlayImageFilter.h>
#include <itkAddImageFilter.h>
#include <itkOpenCVImageBridge.h>

// includes from OpenCV
#include <opencv2/opencv.hpp>

constexpr unsigned int Dimension = 2;
using PixelType = unsigned char;
using ImageType = itk::Image< PixelType, Dimension >;

constexpr unsigned char labelValMask = 255;     // itk::NumericTraits<PixelType>::max()
constexpr unsigned char labelValSpeed = 128;
constexpr unsigned char labelTerminalStart = 192;
constexpr unsigned char labelTerminalEnd = 192;

template <class ImageType_Rfn>
static int ReadImageFile( const std::string &filename, typename ImageType_Rfn::Pointer &imgRead )
{
    using ReaderType = itk::ImageFileReader< ImageType_Rfn >;
    typename ReaderType::Pointer reader = ReaderType::New();
    reader->SetFileName( filename );
    reader->Update();

    imgRead = reader->GetOutput();
    imgRead->DisconnectPipeline();

    return EXIT_SUCCESS;
}

template <class ImageType_Wfn>
static int WriteImageFile( const std::string &filename, const typename ImageType_Wfn::Pointer &imgWrite )
{
    using WriterType = itk::ImageFileWriter< ImageType_Wfn >;
    typename WriterType::Pointer writer = WriterType::New();
    writer->SetFileName( filename );
    writer->SetInput( imgWrite );
    writer->Update();

    return EXIT_SUCCESS;
}

template <class ImageType_RVMfn>
static void ReadVesselMask(const std::string &filenameMask, typename ImageType_RVMfn::Pointer &mask)
{
    using PixelTypeMask = itk::RGBPixel<unsigned char>;
    using ImageTypeMask = itk::Image< PixelTypeMask, Dimension >;

    using ReaderTypeMask = itk::ImageFileReader<ImageTypeMask> ;
    ReaderTypeMask::Pointer readerMask = ReaderTypeMask::New();

    readerMask->SetFileName( filenameMask );
    readerMask->Update();

    ImageTypeMask::Pointer imgMask = readerMask->GetOutput();
    imgMask->DisconnectPipeline();

    typename ImageType_RVMfn::RegionType region = imgMask->GetLargestPossibleRegion();
    mask->SetRegions(region);
    mask->Allocate();

    itk::ImageRegionIterator<ImageType_RVMfn> imageIterator( mask, region );

    // mask yellow pixel(255, 255, 0)
    ImageTypeMask::PixelType pixMaskVal;
    pixMaskVal[0] = 255;
    pixMaskVal[1] = 255;
    pixMaskVal[2] = 0;
    while(!imageIterator.IsAtEnd())
    {
        ImageTypeMask::IndexType pixIdx = static_cast<ImageTypeMask::IndexType>(imageIterator.GetIndex());
        ImageTypeMask::PixelType pixVal = imgMask->GetPixel(pixIdx);

        if (pixVal == pixMaskVal)
        {
            imageIterator.Set(itk::NumericTraits<PixelType>::max());    // labelValMask
        }
        else
        {
            imageIterator.Set(itk::NumericTraits<PixelType>::Zero);
        }
        ++imageIterator;
    }
}

template <class ImageTypeDistMapNMPfn>
static void GetNearMaxPointFromVesselMaskDistMap( const typename ImageTypeDistMapNMPfn::Pointer &imgMaskDistMap,
                                                  const typename ImageTypeDistMapNMPfn::IndexType &idxStart,
                                                  const typename ImageTypeDistMapNMPfn::IndexType &idxEnd,
                                                  unsigned int size_near,
                                                  typename ImageTypeDistMapNMPfn::IndexType &idxPathStart,
                                                  typename ImageTypeDistMapNMPfn::IndexType &idxPathEnd )
{
    // update path points more inner postion from border
    // terminal points(start, end) must be located in mask with a gap from border

    cv::Mat imgMaskDistMapMat = itk::OpenCVImageBridge::ITKImageToCVMat< ImageTypeDistMapNMPfn >( imgMaskDistMap );

    cv::Point2i ptSrc[2] = { cv::Point2i(int(idxStart[0]), int(idxStart[1])),
                             cv::Point2i(int(idxEnd[0]), int(idxEnd[1]))};
    cv::Point2i ptMax[2];

    for ( int i = 0; i < 2; i++ )
    {
        int size_near_impl = std::max(int(size_near), 1);

        int cropCorner[4];
        cropCorner[0] = std::max(ptSrc[i].x - size_near_impl, 0);
        cropCorner[1] = std::max(ptSrc[i].y - size_near_impl, 0);
        cropCorner[2] = std::min(ptSrc[i].x + size_near_impl, imgMaskDistMapMat.cols);
        cropCorner[3] = std::min(ptSrc[i].y + size_near_impl, imgMaskDistMapMat.rows);

        cv::Rect cropRect(cropCorner[0],
                          cropCorner[1],
                          cropCorner[2] - cropCorner[0],
                          cropCorner[3] - cropCorner[1]);

        double min;
        double max;
        cv::Point min_loc;
        cv::Point max_loc;

        cv::minMaxLoc(imgMaskDistMapMat(cropRect), &min, &max, &min_loc, &max_loc);

        // set output values
        ptMax[i].x = cropRect.x + max_loc.x;
        ptMax[i].y = cropRect.y + max_loc.y;
    }

    idxPathStart[0] = ptMax[0].x;
    idxPathStart[1] = ptMax[0].y;

    idxPathEnd[0] = ptMax[1].x;
    idxPathEnd[1] = ptMax[1].y;
}

template <class ImageTypeTPFVMfn>
static bool GetTerminalPointFromVesselMask( const typename ImageTypeTPFVMfn::Pointer &imgMask,
                                            typename ImageTypeTPFVMfn::IndexType &idxStart,
                                            typename ImageTypeTPFVMfn::IndexType &idxEnd )
{
    cv::Mat imgMaskMat = itk::OpenCVImageBridge::ITKImageToCVMat< ImageType >( imgMask );

    int maxCorners = 5;
    double qualityLevel = 0.01;
    double minDistance = 5;
    int blockSize = 3;
    int gradientSize = 3;
    bool useHarrisDetector = false;
    double k = 0.04;

    std::vector<cv::Point2i> corners;
    cv::goodFeaturesToTrack(imgMaskMat,
                            corners,
                            maxCorners,
                            qualityLevel,
                            minDistance,
                            cv::Mat(),
                            blockSize,
                            gradientSize,
                            useHarrisDetector,
                            k);

    // get all distance of corners
    using DistIdxType = struct {
        size_t dist;            // distance of corner point
        size_t idxco_begin;     // begin index of corner point
        size_t idxco_end;       // end index of corner point
    };

    std::vector<DistIdxType> dist_corners;
    for( size_t i = 0; i < corners.size(); i++ )
    {
        for( size_t j = i+1; j < corners.size(); j++ )
        {
            size_t dist = size_t(abs(corners[i].x - corners[j].x) + abs(corners[i].y - corners[j].y));
            DistIdxType dist_idx = {dist, i, j};
            dist_corners.push_back(dist_idx);
        }
    }

    // get terminal points
    std::sort(dist_corners.begin(), dist_corners.end(),
              [](const DistIdxType& a, const DistIdxType& b) -> bool
                {
                    return a.dist < b.dist;
                });

    std::vector<cv::Point2i> terminals;
    std::vector<size_t> idxco_lines;
    size_t cnt_lines = 0;
    for( size_t i = 0; i < dist_corners.size(); i++ )
    {
        if (cnt_lines < 2)
        {
            size_t idxco_begin = dist_corners[i].idxco_begin;
            size_t idxco_end = dist_corners[i].idxco_end;
            if ((idxco_lines.end() == std::find(idxco_lines.begin(), idxco_lines.end(), idxco_begin))
                && (idxco_lines.end() == std::find(idxco_lines.begin(), idxco_lines.end(), idxco_end)))
            {
                idxco_lines.push_back(idxco_begin);
                idxco_lines.push_back(idxco_end);
                cnt_lines++;

                cv::Point2i term = {(corners[idxco_begin].x + corners[idxco_end].x)/2,
                                    (corners[idxco_begin].y + corners[idxco_end].y)/2};
                terminals.push_back(term);
            }
        }
        else
        {
            break;
        }
    }

    if ( terminals.size() < 2 )
    {
        return false;
    }

    // set output values
    idxStart[0] = terminals[0].x;
    idxStart[1] = terminals[0].y;

    idxEnd[0] = terminals[1].x;
    idxEnd[1] = terminals[1].y;

    return true;
}

using MaskShape = enum
{
    Squre = 0,
    Circle = 1,
};

template <class ImageType_CSMfn>
static void CreateShapedMask( typename ImageType_CSMfn::Pointer imgMask,
                              typename ImageType_CSMfn::SizeType sizeImg,
                              typename ImageType_CSMfn::PixelType valMask,
                              typename ImageType_CSMfn::IndexType idxCenter,
                              unsigned int sizeMask,
                              MaskShape shape)
{
    // Create a black image with a white square
    typename ImageType_CSMfn::IndexType start;
    start.Fill(0);

    typename ImageType_CSMfn::RegionType region;
    region.SetSize(sizeImg);
    region.SetIndex(start);
    imgMask->SetRegions(region);
    imgMask->Allocate();
    imgMask->FillBuffer(0);

    int rgnLeft = int(idxCenter[0] - sizeMask);
    int rgnTop = int(idxCenter[1] - sizeMask);
    int rgnRight = int(idxCenter[0] + sizeMask);
    int rgnBottom = int(idxCenter[1] + sizeMask);

    itk::ImageRegionIterator<ImageType_CSMfn> imageIterator(imgMask, imgMask->GetLargestPossibleRegion());
    // Make squares
    while(!imageIterator.IsAtEnd())
    {
        typename ImageType_CSMfn::IndexType idxCur = imageIterator.GetIndex();

        if((idxCur[0] >= rgnLeft && idxCur[0] <= rgnRight)
           && (idxCur[1] >= rgnTop && idxCur[1] <= rgnBottom))
        {
            if (shape == MaskShape::Circle)
            {
                double dist = std::sqrt(std::pow(idxCur[0] - idxCenter[0], 2)
                                        + std::pow(idxCur[1] - idxCenter[1], 2) );

                if (std::round(dist) <= sizeMask)
                {
                    imageIterator.Set(valMask);
                }
            }
            else // (shape == MaskShape::Squre)
            {
                imageIterator.Set(valMask);
            }
        }

      ++imageIterator;
    }
}

using PlotPointType = std::pair<int, int>;
using PlotPointListType = std::vector<PlotPointType>;

static void plotLineLowPts(PlotPointListType &out_list_pt,
                           int x0, int y0, int x1, int y1)
{
    int dx = x1 - x0;
    int dy = y1 - y0;
    int yi = 1;
    if (dy < 0)
    {
        yi = -1;
        dy = -dy;
    }
    int D = 2*dy - dx;
    int y = y0;

    for (int x = x0; x <= x1; x++)
    {
        out_list_pt.push_back({x, y});
        if (D > 0)
        {
            y = y + yi;
            D = D - 2*dx;
        }
        D = D + 2*dy;
    }
}

static void plotLineHighPts(PlotPointListType &out_list_pt,
                            int x0, int y0, int x1, int y1)
{
    int dx = x1 - x0;
    int dy = y1 - y0;
    int xi = 1;
    if (dx < 0)
    {
        xi = -1;
        dx = -dx;
    }
    int D = 2*dx - dy;
    int x = x0;

    for (int y = y0; y <= y1; y++)
    {
        out_list_pt.push_back({x, y});
        if (D > 0)
        {
            x = x + xi;
            D = D - 2*dy;
        }
        D = D + 2*dx;
    }
}

static void plotLinePts(PlotPointListType &out_list_pt,
                        int x0, int y0, int x1, int y1)
{
    bool reverse = false;

    if (abs(y1 - y0) < abs(x1 - x0))
    {
        if (x0 > x1)
        {
            plotLineLowPts(out_list_pt, x1, y1, x0, y0);
            reverse = true;
        }
        else
        {
            plotLineLowPts(out_list_pt, x0, y0, x1, y1);
        }
    }
    else
    {
        if (y0 > y1)
        {
            plotLineHighPts(out_list_pt, x1, y1, x0, y0);
            reverse = true;
        }
        else
        {
            plotLineHighPts(out_list_pt, x0, y0, x1, y1);
        }
    }

    if (reverse)
    {
        // reverse list
        PlotPointListType::iterator first = out_list_pt.begin();
        PlotPointListType::iterator last = out_list_pt.end();
        while ((first != last) && (first != --last))
        {
            std::iter_swap(first++, last);
        }
    }
}

int get_vessel_info(const std::string &input_file,
                    const std::string &output_file,
                    bool reverse,
                    const std::string &debug_img_file,
                    const std::string &debug_overlay_img_file,
                    const int path_start_pos[2],
                    const int path_end_pos[2])
{
    double TerminationValue = 0.5;
    unsigned int NumberOfIterations = 1000;

    {
        std::ostringstream ost;
        ost << "== result ==";

        qInfo() << ost.str().c_str();
        std::cerr << ost.str() << std::endl;
    }

    try
    {
        std::string vesl_input_file = input_file;
        std::string vesl_output_file = output_file;
        std::string vesl_debug_img_file = debug_img_file;
        std::string vesl_debug_overlay_img_file = debug_overlay_img_file;

        std::string current_dir = itksys::SystemTools::GetCurrentWorkingDirectory();

        if ( !itksys::SystemTools::FileIsFullPath(vesl_input_file) )
        {
            vesl_input_file = current_dir + "/" + vesl_input_file;
            if (!itksys::SystemTools::FileExists(vesl_input_file.c_str(), true))
            {
                std::string errormsg("error: input file not exist");
                qCritical() << errormsg.c_str();
                std::cerr << errormsg << std::endl;
                throw std::runtime_error( errormsg );
            }
        }

        if ( !itksys::SystemTools::FileIsFullPath(vesl_output_file) )
        {
            vesl_output_file = current_dir + "/" + vesl_output_file;
        }

        if ( !vesl_debug_img_file.empty() && !itksys::SystemTools::FileIsFullPath(vesl_debug_img_file) )
        {
            vesl_debug_img_file = current_dir + "/" + vesl_debug_img_file;
        }

        if ( !vesl_debug_overlay_img_file.empty() && !itksys::SystemTools::FileIsFullPath(vesl_debug_overlay_img_file) )
        {
            vesl_debug_overlay_img_file = current_dir + "/" + vesl_debug_overlay_img_file;
            if (!itksys::SystemTools::FileExists(vesl_debug_overlay_img_file.c_str(), true))
            {
                std::string errormsg("error: debug overlay file not exist");
                qCritical() << errormsg.c_str();
                std::cerr << errormsg << std::endl;
                throw std::runtime_error( errormsg );
            }
        }

        itk::PNGImageIOFactory::RegisterOneFactory();
        itk::JPEGImageIOFactory::RegisterOneFactory();
        itk::BMPImageIOFactory::RegisterOneFactory();

        // read mask
        ImageType::Pointer imgMask = ImageType::New();
        ReadVesselMask<ImageType>( vesl_input_file, imgMask );

        // create contour outline of mask
        using BinaryContourImageFilterType = itk::BinaryContourImageFilter< ImageType, ImageType >;
        BinaryContourImageFilterType::Pointer binaryContourImageFilter = BinaryContourImageFilterType::New();
        binaryContourImageFilter->SetInput( imgMask );
        binaryContourImageFilter->SetForegroundValue(0);
        binaryContourImageFilter->SetBackgroundValue(itk::NumericTraits<PixelType>::max());

        using InvertIntensityImageFilterType = itk::InvertIntensityImageFilter< ImageType >;
        InvertIntensityImageFilterType::Pointer invertIntensityImageFilter = InvertIntensityImageFilterType::New();
        invertIntensityImageFilter->SetInput( binaryContourImageFilter->GetOutput() );

        // get distance map from contour outline
        using PixelTypeDistMap = float;
        using ImageTypeDistMap = itk::Image< PixelTypeDistMap, Dimension >;

        using DistanceMapImageFilterType = itk::DanielssonDistanceMapImageFilter< ImageType, ImageTypeDistMap  > ;
        DistanceMapImageFilterType::Pointer distMapImageFilter = DistanceMapImageFilterType::New();
        distMapImageFilter->SetInput( invertIntensityImageFilter->GetOutput() );
        distMapImageFilter->InputIsBinaryOn();

        // select only mask from distance map
        using MaskFilterType = itk::MaskImageFilter< ImageTypeDistMap, ImageType >;
        MaskFilterType::Pointer maskFilter = MaskFilterType::New();
        maskFilter->SetInput( distMapImageFilter->GetOutput() );
        maskFilter->SetMaskImage( imgMask );

        maskFilter->Update();

        ImageTypeDistMap::Pointer imgDistMap = maskFilter->GetOutput();
        imgDistMap->DisconnectPipeline();

        // shift up distance map values, if dist value < 1.0( > TerminationValue), speed path(centerline) could be blocked.
        // if maximum of dist was too small, speed path more likely shortest path.
        const float min_DistPathMap = 1.0;
        const float max_DistPathMap = 20.0;

        using PixelTypeSpeed = float;
        using ImageTypeSpeed = itk::Image< PixelTypeSpeed, Dimension >;

        using RescaleIntensityFilterType = itk::RescaleIntensityImageFilter< ImageTypeDistMap, ImageTypeSpeed >;
        RescaleIntensityFilterType::Pointer rescaleFilter = RescaleIntensityFilterType::New();
        rescaleFilter->SetInput( imgDistMap );
        rescaleFilter->SetOutputMinimum(min_DistPathMap);
        rescaleFilter->SetOutputMaximum(max_DistPathMap);

        // set background value to 0.0
        MaskFilterType::Pointer maskFilter2 = MaskFilterType::New();
        maskFilter2->SetInput( rescaleFilter->GetOutput() );
        maskFilter2->SetMaskImage( imgMask );

        maskFilter2->Update();

        ImageTypeDistMap::Pointer imgDistPathMap = maskFilter2->GetOutput();
        imgDistPathMap->DisconnectPipeline();

        // get centerline path with distance map
        using PathType = itk::PolyLineParametricPath< Dimension >;
        using PathFilterType = itk::SpeedFunctionToPathFilter< ImageTypeSpeed, PathType >;
        using CoordRepType = typename PathFilterType::CostFunctionType::CoordRepType;

        // create interpolator for speed path filter
        using InterpolatorType = itk::LinearInterpolateImageFunction<ImageTypeSpeed, CoordRepType>;
        InterpolatorType::Pointer interp = InterpolatorType::New();

        // create cost function for speed path filter
        PathFilterType::CostFunctionType::Pointer cost = PathFilterType::CostFunctionType::New();
        cost->SetInterpolator( interp );

        // create GradientDescentOptimizer for speed path filter
        using OptimizerType = itk::GradientDescentOptimizer;
        OptimizerType::Pointer optimizer = OptimizerType::New();
        optimizer->SetNumberOfIterations( NumberOfIterations );

        // get begin/end point of path
        ImageType::IndexType idxStart = {path_start_pos[0], path_start_pos[1]};
        ImageType::IndexType idxEnd = {path_end_pos[0], path_end_pos[1]};

        const int path_pos_empty[2] = {0, 0};
        if (memcmp(path_start_pos, path_pos_empty, sizeof(path_pos_empty)) == 0
           || memcmp(path_end_pos, path_pos_empty, sizeof(path_pos_empty)) == 0)
        {
            ImageType::IndexType idxTemStart;
            ImageType::IndexType idxTemEnd;

            if ( GetTerminalPointFromVesselMask<ImageType>(imgMask,
                                                           idxTemStart,
                                                           idxTemEnd) )
            {
                // in general, start point starts from upper side(compare y value)
                if (idxTemStart[1] > idxTemEnd[1])
                {
                    ImageType::IndexType idxTmp;

                    idxTmp = idxTemStart;
                    idxTemStart = idxTemEnd;
                    idxTemEnd = idxTmp;
                }

                if (memcmp(path_start_pos, path_pos_empty, sizeof(path_pos_empty)) == 0)
                {
                    idxStart = idxTemStart;
                }

                if (memcmp(path_end_pos, path_pos_empty, sizeof(path_pos_empty)) == 0)
                {
                    idxEnd = idxTemEnd;
                }
            }
            else
            {
                std::string errormsg("error: failed in getting begin end point of path from mask image");
                qCritical() << errormsg.c_str();
                throw std::runtime_error( errormsg );
            }
        }

        if (reverse)
        {
            ImageType::IndexType idxTmp;

            idxTmp = idxStart;
            idxStart = idxEnd;
            idxEnd = idxTmp;
        }

        {
            std::ostringstream ost;
            ost << "centerline path start: " << idxStart[0] << ','  << idxStart[1] << '\n';
            ost << "centerline path end: " << idxEnd[0] << ','  << idxEnd[1];

            qInfo() << ost.str().c_str();
            std::cerr << ost.str() << std::endl;
        }

        // get pathbegin/pathend point of path
        ImageType::IndexType idxPathStart;
        ImageType::IndexType idxPathEnd;
        const int size_near_max = 3;

        GetNearMaxPointFromVesselMaskDistMap<ImageTypeDistMap>(imgDistMap, idxStart, idxEnd, size_near_max,
                                                               idxPathStart, idxPathEnd);

        // allocate speed path image
        ImageType::Pointer output;
        output = ImageType::New();
        output->SetRegions( imgMask->GetLargestPossibleRegion() );
        output->SetSpacing( imgMask->GetSpacing() );
        output->SetOrigin( imgMask->GetOrigin() );
        output->Allocate( );
        output->FillBuffer( itk::NumericTraits<PixelType>::Zero );

        // rasterize path
        using PathIteratorType = itk::PathIterator< ImageType, PathType >;

        using PathDataType = struct
        {
            ImageType::IndexType idx;
            PixelTypeDistMap dist;
            char dummy[4]; // for padding
        };
        using PathDataListType = std::vector< PathDataType >;
        PathDataListType lstPathData;

        // append start point to path data
        lstPathData.push_back({idxStart, 0.0, {0,}});

        ImageType::IndexType idxPathReStart = idxPathStart;
        // create speed path
        int restartCount = 0;
        while(true)
        {
            PathDataListType lstPathDataCur;

            PathFilterType::Pointer pathFilter = PathFilterType::New();
            pathFilter->SetInput( imgDistPathMap );
            pathFilter->SetCostFunction( cost );
            pathFilter->SetOptimizer( optimizer );
            pathFilter->SetTerminationValue( TerminationValue );

            using PointType = itk::Point< double, Dimension >;
            using PathInfoType = itk::SpeedFunctionPathInformation<PointType>;

            PathInfoType::Pointer info = PathInfoType::New();

            // speed path start from end point
            PointType ptPath;
            ptPath[0] = double(idxPathReStart[0]);
            ptPath[1] = double(idxPathReStart[1]);
            info->SetEndPoint( ptPath );

            ptPath[0] = double(idxPathEnd[0]);
            ptPath[1] = double(idxPathEnd[1]);
            info->SetStartPoint( ptPath );

            pathFilter->AddPathInformation( info );

            // compute the speed path
            itk::TimeProbe time;
            time.Start( );
            pathFilter->Update( );
            time.Stop( );
            qInfo() << "path compute time(sec): "<< time.GetMean();

            size_t added_PathDataCur = 0;
            for (unsigned int i=0; i < pathFilter->GetNumberOfOutputs(); i++)
            {
                // get the path,
                PathType::Pointer path = pathFilter->GetOutput( i );

                // check path is valid
                if ( path->GetVertexList()->Size() == 0 )
                {
                    std::ostringstream ost;
                    ost << "warning: path " << i << " contains no points";
                    qWarning() << ost.str().c_str();
                    std::cerr << ost.str() << std::endl;
                    continue;
                }

                // iterate path and convert to image
                PathIteratorType it( output, path );
                for (it.GoToBegin(); !it.IsAtEnd(); ++it)
                {
                    PathDataType    dataPath;

                    dataPath.idx = it.GetIndex();
                    dataPath.dist = imgDistMap->GetPixel(dataPath.idx);
                    if (dataPath.dist > 0)
                    {
                        lstPathDataCur.push_back(dataPath);

                        imgDistPathMap->SetPixel(dataPath.idx, min_DistPathMap);
                        added_PathDataCur++;
                    }
                }
            }

            bool reachPathEnd = false;
            if (added_PathDataCur > 0)
            {
                // first 5 point will be removed and filled with line, it will make more smooth centerline
                if (lstPathDataCur.size() > 5)
                {
                    lstPathDataCur.erase(lstPathDataCur.begin());
                    lstPathDataCur.erase(lstPathDataCur.begin());
                    lstPathDataCur.erase(lstPathDataCur.begin());
                    lstPathDataCur.erase(lstPathDataCur.begin());
                    lstPathDataCur.erase(lstPathDataCur.begin());
                }

                const PathDataType &dataPathStartCur = *(lstPathDataCur.begin());
                const PathDataType &dataPathEndCur = *(--(lstPathDataCur.end()));
                bool appendPathCur = false;

                // check the path was reached to the idxPathEnd
                if ( std::abs(dataPathEndCur.idx[0] - idxPathEnd[0]) <= 5
                     && std::abs(dataPathEndCur.idx[1] - idxPathEnd[1]) <= 5 )
                {
                    appendPathCur = true;

                    // reached to the end
                    reachPathEnd = true;
                }
                else
                {
                    // not reached to the end
                    if (added_PathDataCur > 3)
                    {
                        appendPathCur = true;
                    }
                }

                // append current path
                if (appendPathCur)
                {
                    // fill the gap between lines
                    {
                        // last 5 point will be removed and filled with line, it will make more smooth centerline
                        if (lstPathData.size() > 5)
                        {
                            lstPathData.erase(--lstPathData.end());
                            lstPathData.erase(--lstPathData.end());
                            lstPathData.erase(--lstPathData.end());
                            lstPathData.erase(--lstPathData.end());
                            lstPathData.erase(--lstPathData.end());
                        }

                        const PathDataType &dataPathEnd = *(--(lstPathData.end()));

                        PlotPointListType list_pt;
                        plotLinePts(list_pt,
                                    static_cast<int>(dataPathEnd.idx[0]), static_cast<int>(dataPathEnd.idx[1]),
                                    static_cast<int>(dataPathStartCur.idx[0]), static_cast<int>(dataPathStartCur.idx[1]));

                        // ignore start, end point
                        if (list_pt.size() > 2)
                        {
                            PlotPointListType::iterator it_begin = ++list_pt.begin();
                            PlotPointListType::iterator it_end = --list_pt.end();

                            for (PlotPointListType::iterator it = it_begin ; it != it_end; ++it)
                            {
                                PathDataType    dataPath;
                                dataPath.idx[0] = (*it).first;
                                dataPath.idx[1] = (*it).second;
                                dataPath.dist = imgDistMap->GetPixel(dataPath.idx);
                                if (dataPath.dist > 0)
                                {
                                    lstPathData.push_back(dataPath);
                                }
                            }
                        }
                    }

                    lstPathData.insert(lstPathData.end(), lstPathDataCur.begin(), lstPathDataCur.end());

                    // fill the gap between current path end and end point
                    if (reachPathEnd)
                    {
                        PlotPointListType list_pt;
                        plotLinePts(list_pt,
                                    static_cast<int>(dataPathEndCur.idx[0]), static_cast<int>(dataPathEndCur.idx[1]),
                                    static_cast<int>(idxEnd[0]), static_cast<int>(idxEnd[1]));

                        // ignore start, end point
                        if (list_pt.size() > 2)
                        {
                            PlotPointListType::iterator it_begin = ++list_pt.begin();
                            PlotPointListType::iterator it_end = --list_pt.end();

                            for (PlotPointListType::iterator it = it_begin ; it != it_end; ++it)
                            {
                                PathDataType    dataPath;
                                dataPath.idx[0] = (*it).first;
                                dataPath.idx[1] = (*it).second;
                                dataPath.dist = imgDistMap->GetPixel(dataPath.idx);

                                lstPathData.push_back(dataPath);
                            }
                        }
                    }

                    std::ostringstream ost;
                    ost << "centerline: "
                        << "from (" << dataPathStartCur.idx[0] << ','  << dataPathStartCur.idx[1]<< ") "
                        << "to (" << dataPathEndCur.idx[0] << ','  << dataPathEndCur.idx[1]<< ')';

                    qInfo() << ost.str().c_str();
                    std::cerr << ost.str() << std::endl;
                }
                else
                {
                    std::ostringstream ost;
                    ost << "abort centerline: "
                        << "from (" << dataPathStartCur.idx[0] << ','  << dataPathStartCur.idx[1]<< ") "
                        << "to (" << dataPathEndCur.idx[0] << ','  << dataPathEndCur.idx[1]<< ')';

                    qInfo() << ost.str().c_str();
                    std::cerr << ost.str() << std::endl;
                }
            }

            if (reachPathEnd)
            {
                break;
            }
            else
            {
                if (++restartCount < (((2*size_near_max+1) * (2*size_near_max+1)) - 1))
                {
                    if (lstPathData.size() > 0)
                    {
                        // abort current path, retry another path
                        const PathDataType &dataPathEnd = *(--(lstPathData.end()));

                        ImageType::IndexType idxPathDummy;
                        GetNearMaxPointFromVesselMaskDistMap<ImageTypeDistMap>(imgDistPathMap, dataPathEnd.idx, idxPathEnd, size_near_max,
                                                                               idxPathReStart, idxPathDummy);
                        imgDistPathMap->SetPixel(idxPathReStart, min_DistPathMap);
                    }
                    else
                    {
                        std::string errormsg("error: failed to create the path from start point, check start point.");
                        qCritical() << errormsg.c_str();
                        std::cerr << errormsg << std::endl;
                        break;
                    }
                }
                else
                {
                    std::string errormsg("error: failed to reach to the path end point");
                    qCritical() << errormsg.c_str();
                    std::cerr << errormsg << std::endl;
                    break;
                }
            }
        }

        // append end point to path data
        lstPathData.push_back({idxEnd, 0.0, {0,}});

        // in head and tail parts of speed path, because of mask end face, radius start from 0.0 and end with 0.0
        // we will fill this parts with max radius value, to ignore effect of end face.
        {
            PixelTypeDistMap max_dist;
            size_t cur_idx;

            max_dist = 0.0;
            for(cur_idx = 0; cur_idx < 20 && cur_idx < lstPathData.size() ; cur_idx++)
            {
                if (lstPathData[cur_idx].dist >= max_dist)
                {
                    max_dist = lstPathData[cur_idx].dist;
                }
                else
                {
                    break;
                }
            }

            for(;;cur_idx--)
            {
                lstPathData[cur_idx].dist = max_dist;

                if (cur_idx == 0)
                {
                    break;
                }
            }

            max_dist = 0.0;
            size_t min_idx = lstPathData.size() - std::min(lstPathData.size(), static_cast<size_t>(20));
            for(cur_idx = (lstPathData.size()-1); cur_idx >= min_idx; cur_idx--)
            {
                if (lstPathData[cur_idx].dist >= max_dist)
                {
                    max_dist = lstPathData[cur_idx].dist;
                }
                else
                {
                    break;
                }
            }

            for(;cur_idx < lstPathData.size(); cur_idx++)
            {
                lstPathData[cur_idx].dist = max_dist;
            }
        }

        // create speed path csv file and image filter
        std::ofstream ofcsv;

        ofcsv.open(vesl_output_file, std::ofstream::out);
        ofcsv << "center_x" << "," << "center_y" << "," << "radius" << ",";
        ofcsv << "start_x" << "," << "start_y" << ",";
        ofcsv << "end_x" << "," << "end_y" << std::endl;

        for(PathDataListType::iterator it = lstPathData.begin(); it != lstPathData.end(); it++)
        {
            ofcsv << it->idx[0] << "," << it->idx[1] << "," << (round(it->dist * 100)/100) << ",";
            ofcsv << idxStart[0] << ','  << idxStart[1] << ",";
            ofcsv << idxEnd[0] << ',' << idxEnd[0] << std::endl;
            output->SetPixel(it->idx, labelValSpeed);
        }

        ofcsv.close();

        {
            std::ostringstream ost;
            ost << "centerline path points: " << lstPathData.size();
            qInfo() << ost.str().c_str();
            std::cerr << ost.str() << std::endl;
        }

        if (!vesl_debug_img_file.empty())
        {
            // add speed path(centerline) and mask to label
            using LabelType = PixelType;
            using LabelObjectType = itk::LabelObject< LabelType, Dimension >;
            using LabelMapType = itk::LabelMap< LabelObjectType >;

            using ConverterType = itk::LabelImageToLabelMapFilter< ImageType, LabelMapType >;
            ConverterType::Pointer converter = ConverterType::New();

            ImageType::SizeType imgTerminalsSize = imgMask->GetLargestPossibleRegion().GetSize();
            ImageType::Pointer imgTmStart = ImageType::New();
            CreateShapedMask<ImageType>(imgTmStart,
                                        imgTerminalsSize,
                                        labelTerminalStart,
                                        idxStart,
                                        5,
                                        MaskShape::Circle);

            using AddImageFilterType = itk::AddImageFilter<ImageType, ImageType >;
            AddImageFilterType::Pointer addFilterStart = AddImageFilterType::New();
            addFilterStart->SetInput1( output );
            addFilterStart->SetInput2( imgTmStart );

            ImageType::Pointer imgTmEnd = ImageType::New();
            CreateShapedMask<ImageType>(imgTmEnd,
                                        imgTerminalsSize,
                                        labelTerminalEnd,
                                        idxEnd,
                                        5,
                                        MaskShape::Squre);

            AddImageFilterType::Pointer addFilterEnd = AddImageFilterType::New();
            addFilterEnd->SetInput1( addFilterStart->GetOutput() );
            addFilterEnd->SetInput2( imgTmEnd );

            // create labeled image
            AddImageFilterType::Pointer addFilterMask = AddImageFilterType::New();
            ImageType::Pointer imgSrc = ImageType::New();
            if (!vesl_debug_overlay_img_file.empty())
            {
                addFilterMask = AddImageFilterType::New();

                addFilterMask->SetInput1( addFilterEnd->GetOutput() );
                addFilterMask->SetInput2( imgMask );

                converter->SetInput( addFilterMask->GetOutput() );

                ReadImageFile< ImageType >( vesl_debug_overlay_img_file, imgSrc);
            }
            else
            {
                converter->SetInput( addFilterEnd->GetOutput() );

                imgSrc = imgMask;
            }

            using FilterType = itk::LabelMapOverlayImageFilter< LabelMapType, ImageType >;
            FilterType::Pointer filter = FilterType::New();
            filter->SetInput( converter->GetOutput() );
            filter->SetFeatureImage( imgSrc );
            filter->SetOpacity( 0.5 );

            // write debug image file
            WriteImageFile<FilterType::OutputImageType>( vesl_debug_img_file, filter->GetOutput() );
        }
    }
    catch( itk::ExceptionObject & err )
    {
        qCritical() << "loc: "<< err.GetLocation() << "dec: " << err.GetDescription();
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}


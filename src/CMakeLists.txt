cmake_minimum_required(VERSION 3.16)
project(mp_qca_centerline)

set(CMAKE_CXX_STANDARD 17)

#add_subdirectory(src)
#add_executable(mp_qca_centerline main.cpp)

set(TARGET_NAME mp_qca_centerline)

# nltk_c
set(SOURCE_FILES
    main.cpp
    vessel_info.cpp
)

add_executable(${TARGET_NAME} ${SOURCE_FILES})

target_link_libraries(${TARGET_NAME} ${LIBRARY_LIST})

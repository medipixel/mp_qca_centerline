#include <QCoreApplication>
#include <QCommandLineParser>
#include <QString>
#include <QStringList>
#include <QRegExp>
#include <QPoint>
#include <QDir>

#include <iostream>

#include "qxlogger.h"

#include "vessel_info.h"

#define STR_APP_VER "ver 1.0.0.1"
#define STR_APP_DESC "Get vessel centerline positions and radius from input mask image"

bool parse_point_from_string(const QString& input, QPoint& output)
{
    const QRegExp rx("[, ]"); // match a comma or a space
    const QRegExp rd("\\d*");  // a digit (\d), zero or more times (*)

    bool ret = false;

    QStringList list = input.split(rx, QString::SkipEmptyParts);

    if (list.size() >= 2
        && rd.exactMatch(list.at(0))
        && rd.exactMatch(list.at(1)))
    {
        output = QPoint(list[0].toInt(), list[1].toInt());
        ret = true;
    }

    return ret;
}

int main(int argc, char *argv[])
{
    int ret = EXIT_SUCCESS;

    QCoreApplication app(argc, argv);
    app.setApplicationVersion(STR_APP_VER);

    initQxLogger("log/get-vessel-info.log", (1* 1024 * 1024), 5);

    qInfo() << QString("process is started");

    QCommandLineParser cmdlineParser;
    QCommandLineOption optReverse({{"r", "reverse"}, "centerline start and end point into reverse."});
    QCommandLineOption optDebugImage({{"d", "debug"}, "output debug centerline image <file>.", "file"});
    QCommandLineOption optDebugOverlayImage({{"o", "overlay"}, "overlay debug centerline image to overlay image <file>.", "file"});
    QCommandLineOption optPathStart({{"s", "pathstart"}, "path start point coordinate <x,y>.", "x,y"});
    QCommandLineOption optPathEnd({{"e", "pathend"}, "path end point coordinate <x,y>.", "x,y"});

    cmdlineParser.setApplicationDescription(STR_APP_DESC);
    cmdlineParser.addHelpOption();
    cmdlineParser.addVersionOption();
    cmdlineParser.addOption(optReverse);
    cmdlineParser.addOption(optDebugImage);
    cmdlineParser.addOption(optDebugOverlayImage);
    cmdlineParser.addOption(optPathStart);
    cmdlineParser.addOption(optPathEnd);
    cmdlineParser.addPositionalArgument("input_file",
                                        "input file, mask IMAGE file");
    cmdlineParser.addPositionalArgument("output_file",
                                        "output file, vessel infomation CSV file");

    cmdlineParser.process(QCoreApplication::arguments());

    const QStringList& params = cmdlineParser.positionalArguments();
    if (params.size() >= 2)
    {
        QString input_file;
        QString output_file;
        bool reverse = false;
        QString debug_img_file;
        QString debug_overlay_img_file;
        QPoint path_start(0, 0);
        QPoint path_end(0, 0);

        input_file = params.at(0);
        output_file = params.at(1);

        if (cmdlineParser.isSet(optReverse))
        {
            reverse = true;
        }

        if (cmdlineParser.isSet(optDebugImage))
        {
            debug_img_file = cmdlineParser.value("debug");
        }

        if (cmdlineParser.isSet(optDebugOverlayImage))
        {
            debug_overlay_img_file = cmdlineParser.value("overlay");
        }

        if (cmdlineParser.isSet(optPathStart))
        {
            QString val = cmdlineParser.value("pathstart");
            if (!parse_point_from_string (val, path_start))
            {
                QString errormsg = QString("pathstart was invalid format, i.e. 1,1 : %1").arg(val);
                qCritical() << errormsg;
                std::cerr << errormsg.toStdString() << std::endl;

                ret = EXIT_FAILURE;
            }
        }

        if (cmdlineParser.isSet(optPathEnd))
        {
            QString val = cmdlineParser.value("pathend");
            if (!parse_point_from_string (val, path_end))
            {
                QString errormsg = QString("pathend was invalid format, i.e. 1,1 : %1").arg(val);
                qCritical() << errormsg;
                std::cerr << errormsg.toStdString() << std::endl;

                ret = EXIT_FAILURE;
            }
        }

        if (ret == EXIT_SUCCESS)
        {
            QString outmsg;

            outmsg += QString("parameter intput file: %1 \n").arg(input_file);
            outmsg += QString("parameter output file: %1 \n").arg(output_file);
            outmsg += QString("parameter reverse \n").arg(reverse);
            outmsg += QString("parameter debug image file: %1 \n").arg(debug_img_file);
            outmsg += QString("parameter debug overlay image file: %1 \n").arg(debug_overlay_img_file);
            if (path_start == QPoint(0, 0))
            {
                outmsg += QString("parameter path start point: None \n");
            }
            else
            {
                outmsg += QString("parameter path start point: %1, %2 \n").arg(path_start.x()).arg(path_start.y());
            }

            if (path_end == QPoint(0, 0))
            {
                outmsg += QString("parameter path end point: None \n");
            }
            else
            {
                outmsg += QString("parameter path end point: %1, %2 \n").arg(path_end.x()).arg(path_end.y());
            }

            outmsg += QString("log output: %1 \n").arg(getQxLoggerPath());

            qInfo() << outmsg;

            std::cerr << outmsg.toStdString() << std::endl;

            // create output path
            {
                QDir dir_output = QFileInfo(output_file).absoluteDir();
                if (!dir_output.exists())
                {
                    dir_output.mkpath(".");
                }

                if (debug_img_file.size() > 0)
                {
                    QDir dir_debug = QFileInfo(debug_img_file).absoluteDir();
                    if (!dir_debug.exists())
                    {
                        dir_debug.mkpath(".");
                    }
                }
            }

            int path_start_pos[2] = {path_start.x(), path_start.y()};
            int path_end_pos[2] = {path_end.x(), path_end.y()};

            ret = get_vessel_info(input_file.toStdString(),
                                  output_file.toStdString(),
                                  reverse,
                                  debug_img_file.toStdString(),
                                  debug_overlay_img_file.toStdString(),
                                  path_start_pos,
                                  path_end_pos);
        }

        if (ret != EXIT_SUCCESS)
        {
            QString errormsg = QString("error occured: %1").arg(ret);
            qCritical() << errormsg;
            std::cerr << errormsg.toStdString() << std::endl;
        }
    }
    else
    {
        QString errormsg("parameter error: parameters <input_file> <output_file> were required");
        qCritical() << errormsg;
        std::cerr << errormsg.toStdString() << std::endl;
    }

    qInfo() << QString("process was ended: %1").arg(ret);

    return ret;
}
